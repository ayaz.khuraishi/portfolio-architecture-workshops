Workshop online at: [https://redhatdemocentral.gitlab.io/portfolio-architecture-workshops](https://redhatdemocentral.gitlab.io/portfolio-architecture-workshops)

![Cover Slide](cover.png)
![Logical diagram](images/lab03-44.png)
![Schematic diagram](images/lab05-63.png)
![Detail diagram](images/lab06-26.png)

